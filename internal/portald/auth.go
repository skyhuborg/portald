/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package ui

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"net/http"
	"strings"
)

var (
	MissingMetadata  = status.Errorf(codes.InvalidArgument, "missing metadata")
	InvalidToken     = status.Errorf(codes.Unauthenticated, "invalid token")
	PermissionDenied = status.Errorf(codes.PermissionDenied, "permission denied")
)

type Token struct {
	Username  *string
	AuthToken *string
}

var tokens = map[string]*Token{}

func GetUserByToken(token_str string) *User {
	token := tokens[token_str]
	return GetUserByName(*token.Username)
}

func UnaryAuthInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {

	new_ctx, _ := authorize(ctx)

	m, err := handler(new_ctx, req)
	if err != nil {
		grpclog.Printf("RPC failed with error %v", err)
	}

	return m, err
}

func login(username string, password string) (string, error) {
	user := GetUserByName(username)
	if user != nil {
		err := bcrypt.CompareHashAndPassword(
			[]byte(user.Password),
			[]byte(password))
		if err == nil {
			token := uuid.New().String()

			tokenStruct := &Token{
				Username:  &user.Username,
				AuthToken: &token,
			}
			tokens[token] = tokenStruct
			return token, nil
		} else {
			grpclog.Printf("Error: %s\n", err)
		}
	}

	return "", fmt.Errorf("Invalid Credentials")

}

func loginCheck(auth_token string) (string, error) {
	if token, ok := tokens[auth_token]; ok {
		return *token.Username, nil
	} else {
		return "", InvalidToken
	}
}

func logout(auth_token string) error {
	if _, ok := tokens[auth_token]; ok {
		delete(tokens, auth_token)
	}
	return nil
}

func authorize(ctx context.Context) (context.Context, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return ctx, status.Errorf(codes.InvalidArgument, "Retrieving metadata failed")
	}

	authtoken, ok := md["authtoken"]
	if !ok {
		return ctx, status.Errorf(codes.Unauthenticated, "Authorization token is not supplied")
	}

	token := authtoken[0]
	username, err := loginCheck(token)

	if err == nil {
		user := GetUserByName(username)
		if user == nil {
			goto unauthorized
		}

		auth_map := map[string]interface{}{
			"username": username,
			"roles":    strings.Split(user.Roles, ","),
		}
		auth_ctx := context.WithValue(ctx, "auth", &auth_map)
		return auth_ctx, nil
	}

unauthorized:
	return ctx, status.Errorf(codes.Unauthenticated, err.Error())
}

func authorizeHttp(r *http.Request) (context.Context, error) {
	ctx := context.Background()

	token := r.Header.Get("auth-token")
	if token == "" {
		return nil, status.Errorf(codes.Unauthenticated, "Authorization token is not supplied")
	}

	username, err := loginCheck(token)

	if err == nil {
		user := GetUserByName(username)
		if user == nil {
			goto unauthorized
		}

		auth_map := map[string]interface{}{
			"username": username,
			"roles":    strings.Split(user.Roles, ","),
		}
		auth_ctx := context.WithValue(ctx, "auth", &auth_map)
		return auth_ctx, nil
	}

unauthorized:
	return ctx, status.Errorf(codes.Unauthenticated, err.Error())

}

func StringInSlice(str string, list []string) bool {
	for _, v := range list {
		if v == str {
			return true
		}
	}
	return false
}

func enforceRole(role string, ctx context.Context) error {
	auth_iface := ctx.Value("auth")
	if auth_iface == nil {
		return PermissionDenied
	}

	auth := *(auth_iface.(*map[string]interface{}))

	if auth == nil {
		return PermissionDenied
	}

	roles, ok := auth["roles"].([]string)
	if !ok {
		return PermissionDenied
	}

	if !StringInSlice("admin", roles) {
		return PermissionDenied
	}
	return nil
}

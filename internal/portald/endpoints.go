/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package ui

import (
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes"
	"github.com/improbable-eng/grpc-web/go/grpcweb"
	pb "gitlab.com/uaptn/proto-portald-go"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"sync"
	"time"
)

type Server struct {
	Handle     *grpc.Server
	ListenPort int
	EnableTls  bool
	TlsKey     string
	TlsCert    string
	DbHost     string
	DbPort     int
	DbUser     string
	DbPassword string
}

var client_pool = sync.Pool{
	New: func() interface{} {
		return mongo_connect()
	},
}

func mongo_connect() *mongo.Client {
	clientOptions := options.Client().ApplyURI("mongodb://uaptn:uaptn123@mongo:27017")
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
	}
	return client
}

func (s *Server) Start() {
	s.Handle = grpc.NewServer(grpc.UnaryInterceptor(UnaryAuthInterceptor))

	pb.RegisterUiServer(s.Handle, s)

	//log.SetLogger(log.New(os.Stdout, "uaptu-ui: ", log.LstdFlags))

	wrappedServer := grpcweb.WrapServer(s.Handle,
		grpcweb.WithOriginFunc(func(origin string) bool {
			return true
		}))

	handler := func(resp http.ResponseWriter, req *http.Request) {
		wrappedServer.ServeHTTP(resp, req)
	}

	httpServer := http.Server{
		Addr:    fmt.Sprintf(":%d", s.ListenPort),
		Handler: http.HandlerFunc(handler),
	}

	if s.EnableTls == true {
		log.Printf("Starting server with TLS on port %d", s.ListenPort)
		if err := httpServer.ListenAndServeTLS(s.TlsCert, s.TlsKey); err != nil {
			log.Fatalf("failed starting http server: %v", err)
		}
	} else {
		log.Printf("Starting server without TLS on port %d", s.ListenPort)
		if err := httpServer.ListenAndServe(); err != nil {
			log.Fatalf("failed starting http server: %v", err)
		}
	}
}

/* AUTH_LOGIN, AUTH_LOGOUT, AUTH_CHECK */
func (s *Server) Auth(
	ctx context.Context,
	request *pb.AuthReq) (*pb.AuthResp, error) {

	status := "success"

	if request.RequestType == pb.AuthType_AUTH_LOGIN {
		if request.Username == "" || request.Password == "" {
			status = "Missing Username or Password"
			goto failure
		}

		token, err := login(request.Username, request.Password)

		if err != nil {
			status = err.Error()
			goto failure
		}

		user := GetUserByName(request.Username)
		roles := user.Roles

		return &pb.AuthResp{
			Status: status,
			Token:  token,
			User: &pb.User{
				Username: user.Username,
				Roles:    roles,
			},
		}, nil
	} else if request.RequestType == pb.AuthType_AUTH_LOGOUT {
		logout(request.Token)
		return &pb.AuthResp{Status: status}, nil
	} else if request.RequestType == pb.AuthType_AUTH_CHECK {
		if request.Token == "" {
			status = "Missing Auth Token"
			goto failure
		}

		_, err := loginCheck(request.Token)

		if err != nil {
			status = err.Error()
			goto failure
		}

		user := GetUserByToken(request.Token)
		roles := user.Roles

		return &pb.AuthResp{
			Status: status,
			Token:  request.Token,
			User: &pb.User{
				Username: user.Username,
				Roles:    roles,
			},
		}, nil
	}

failure:
	return &pb.AuthResp{Status: status}, nil
}

func getUniqueNodeList(c *mongo.Client, ctx context.Context) []string {
	col := c.Database("uaptn").Collection("status")

	res, _ := col.Distinct(ctx, "uuid", bson.D{})

	nodeList := make([]string, len(res))
	for i, v := range res {
		uuid := fmt.Sprint(v)
		if uuid == "" {
			continue
		}
		nodeList[i] = uuid
	}
	return nodeList
}

type GPS struct {
	Time time.Time `bson:"time"`
	Lat  float64   `bson:"lat"`
	Lon  float64   `bson:"lon"`
}

type Status struct {
	Uuid  string `bson:"uuid"`
	Name  string `bson:"name"`
	Build string `bson:"build"`
	Gps   GPS    `bson:"gps"`
}

type fields struct {
	Uuid  int `bson:"uuid"`
	Name  int `bson:"name"`
	Build int `bson:"build"`
	Lat   int `bson:"gps.lat"`
	Lon   int `bson:"gps.lon"`
	Time  int `bson:"gps.time"`
}

func getNodeStatusByUuids(c *mongo.Client, ctx context.Context, ids []string) []Status {
	col := c.Database("uaptn").Collection("status")
	projection := fields{
		Uuid:  1,
		Name:  1,
		Build: 1,
		Lat:   1,
		Lon:   1,
		Time:  1,
	}

	findOptions := options.FindOne()
	findOptions.SetProjection(projection)
	findOptions.SetSort(bson.D{{"_id", -1}})

	results := make([]Status, len(ids))
	var status Status
	for i, id := range ids {
		col.FindOne(context.TODO(), bson.D{{"uuid", id}}, findOptions).Decode(&status)
		results[i] = status
	}
	return results
}

func (s *Server) GetNodeStatus(ctx context.Context, in *pb.GetNodeStatusReq) (*pb.GetNodeStatusResp, error) {
	err := enforceRole("admin", ctx)
	if err != nil {
		log.Printf("GetNodeStatus endpoint called. Rejected: Permission Denied.")
		return nil, err
	}

	log.Printf("GetNodeStatus endpoint called")

	resp := &pb.GetNodeStatusResp{}

	c := client_pool.Get().(*mongo.Client)
	defer client_pool.Put(c)

	// get unique node list
	uuidList := getUniqueNodeList(c, ctx)

	statusList := getNodeStatusByUuids(c, ctx, uuidList)

	for _, status := range statusList {
		ts, _ := ptypes.TimestampProto(status.Gps.Time)
		ns := &pb.NodeStatus{
			Uuid:  status.Uuid,
			Name:  status.Name,
			Build: status.Build,
			Time:  ts,
			Lat:   status.Gps.Lat,
			Lon:   status.Gps.Lon,
		}
		resp.Status = append(resp.Status, ns)
	}

	return resp, nil
}

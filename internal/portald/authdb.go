/*
MIT License
-----------

Copyright (c) 2020 Steve McDaniel

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
package ui

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"log"
	"sync"
)

type User struct {
	gorm.Model
	Username string `gorm:"type:text;unique_index"`
	Password string `gorm:"type:text"`
	Roles    string `gorm:"type:text"`
}

var authDB *gorm.DB
var mutex = &sync.Mutex{}

func getAuthDB() (*gorm.DB, error) {
	var err error

	mutex.Lock()
	if authDB == nil {
		authDB, err = gorm.Open("sqlite3", "/app/auth.db")

		if err != nil {
			log.Printf("Error: %s\n", err)
			return nil, err
		}
	}
	mutex.Unlock()

	return authDB, nil
}

func closeAuthDB() {
	log.Printf("Closing AuthDB...\n")

	mutex.Lock()
	if authDB != nil {
		authDB.Close()
		authDB = nil
	}
	mutex.Unlock()
}

func GetUserByName(username string) *User {
	var user User

	db, err := getAuthDB()
	if err != nil {
		log.Printf("GetUserByName: %s\n", err)
		return nil
	}

	err = db.Where(&User{Username: username}).First(&user).Error
	if err != nil {
		log.Printf("%s for %s\n", err, username)
		return nil
	}

	return &user
}
